Proceso hallarelareadeunrectangulo
	Definir b,h Como Entero
	Escribir "CALCULA EL AREA DE UN RECTANGULO"
	Escribir "ingresa la base (b) del rectangulo"
	Leer b
	Escribir "ingresa la altura (h) del rectangulo"
	Leer h
	//CALCULAMOS EL AREA Y PERIMETRO DEL RECTANGULO//
	AREA<- b*h
	PERIMETRO=(b+h)*2
	Escribir "EL AREA DEL RECTANGULO ES:",AREA
	Escribir "EL PERIMETRO DEL RECTANGULO ES:",PERIMETRO
FinProceso
